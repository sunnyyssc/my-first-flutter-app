import 'package:flutter/material.dart';
import './app_screens/home.dart';

void main(){
  runApp(
    new MaterialApp(
      title: "Exploring UI widgets",
      home: Home()
    )
  );
}